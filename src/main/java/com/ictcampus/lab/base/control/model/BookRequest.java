package com.ictcampus.lab.base.control.model;

public class BookRequest {
    private String title;
    private String author;
    private String ISDB;
    private String Description;

    public String getTitle() {
        return title;
    }

    public void setTitle( final String name ) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor( final String author ) {
        this.author = author;
    }

    public String getISDB() {
        return ISDB;
    }
    public void setISDB( final String ISDB ) {
        this.ISDB = ISDB;
    }
    public String getDescription() {
        return Description;
    }
}

