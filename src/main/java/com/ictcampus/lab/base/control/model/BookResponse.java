package com.ictcampus.lab.base.control.model;

import jdk.jfr.Description;
import lombok.Builder;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;

/**
 * TODO Add Class Description
 *
 * @author Emilio (FEj) Frusciante - Beta80Group
 * @since 1.0.0
 */

public class BookResponse {
	private Long id;
	private String title;
	private String author;
	private String ISDB;
	private String Description;

	public Long getId() {
		return id;
	}

	public void setId( final Long id ) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle( final String title ) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor( final String author ) {
		this.author = author;
	}

	public String getISDB() {return ISDB;}

	public void setISDB( final String ISDB ) {
		this.ISDB = ISDB;
	}

	public String getDescription() {return Description;}

	public void setDescription(final String Description) {this.Description = Description;}

}

