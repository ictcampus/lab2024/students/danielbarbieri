package com.ictcampus.lab.base.control;

import com.ictcampus.lab.base.control.model.WorldResponse;
import com.ictcampus.lab.base.control.model.WorldRequest;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.*;

/**
 * TODO Add Class Description
 *
 * @author Pipposultavolino
 * @since 1.0.13.2
 */

@RestController
@RequestMapping( "/api/v1/words" )
@AllArgsConstructor
@Slf4j
public class WorldController {
	//global value
	private	List<WorldResponse> list =  generateword();

	public WorldController() {
		this.list = generateword();
	}




	@GetMapping(value = "/init", produces = {MediaType.APPLICATION_JSON_VALUE})
	public String init() {
		list = generateword();

		return  "Init OK";
	}


	@GetMapping(value = "", produces = {MediaType.APPLICATION_JSON_VALUE})
	public List<WorldResponse> getWords() {
		log.info( "Restituisco la lista dei pianeti [{}]", list );
		return list;
	}



	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public WorldResponse getWord(@PathVariable(name = "id") Long id) throws ChangeSetPersister.NotFoundException {
		WorldResponse worldResponse = findWorldById(id);
		if (worldResponse == null) {
			log.info("Pianeta con ID [{}] non trovato", id);
			throw new ChangeSetPersister.NotFoundException();
		}
		return worldResponse;
	}




	@PostMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
	public Long createWorld(@RequestBody WorldRequest worldRequest) {
		String newName = worldRequest.getName();

		// Controlla se un oggetto con lo stesso nome esiste già nella lista
		if (doesWorldExistWithName(newName)) {
			log.warn("Il pianeta con nome '{}' esiste già.", newName);
			throw new NameAlreadyExistsException();
		}

		// Genera un nuovo ID e crea il nuovo oggetto WorldResponse
		Long newId = lastId();
		log.info("Creato un nuovo pianeta con ID '{}', nome '{}', sistema '{}'.", newId, newName, worldRequest.getSystem());

		WorldResponse worldResponse = WorldResponse.builder()
				.id(newId)
				.name(newName)
				.system(worldRequest.getSystem())
				.build();

		// Aggiunge il nuovo oggetto alla lista
		list.add(worldResponse);

		return newId;
	}





	@PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public void editWorld(
			@PathVariable(name = "id") Long id,
			@RequestBody WorldRequest worldRequest
	) {
		log.info("Aggiorno il pianeta con ID [{}] e dati [{}]", id, worldRequest);
		updateById(id, worldRequest);
	}

	@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public void deleteWord(@PathVariable(name = "id") Long id) {
		log.info("Cancello il pianeta con ID [{}]", id);

		boolean removed = list.removeIf(worldResponse -> worldResponse.getId().equals(id));
		if (removed) {
			log.info("Pianeta con ID [{}] eliminato correttamente", id);
		} else {
			log.warn("Pianeta con ID [{}] non trovato o già eliminato", id);
		}
	}



	private void updateById(Long id, WorldRequest worldRequest) {
		WorldResponse worldResponse = findWorldById(id);
		if (worldResponse != null) {
			worldResponse.setName(worldRequest.getName());
			worldResponse.setSystem(worldRequest.getSystem());
			// Potresti voler aggiungere ulteriori logica di aggiornamento qui, se necessario
		} else {
			log.warn("Pianeta con ID [{}] non trovato", id);
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Pianeta non trovato");
		}
	}





	private List<WorldResponse> generateword() {
		String[] names = {"Terra", "Marte", "Giove", "Venere", "Saturno"};
		List<WorldResponse> list = new ArrayList<>();
		for (int i = 0; i < names.length; i++) {
			WorldResponse worldResponse = WorldResponse.builder()
					.id((long) (i + 1))
					.name(names[i])
					.system("Sistema solare")
					.build();
			list.add(worldResponse);
		}
		Collections.shuffle(list);
		return list;
	}





	private WorldResponse findWorldById(Long id) {
		Optional<WorldResponse> worldResponseOptional = list.stream()
				.filter(item -> item.getId().equals(id))
				.findFirst();
		return worldResponseOptional.orElse(null);
	}

	private Long lastId() {
		return list.stream()
				.map(WorldResponse::getId)
				.max(Long::compareTo)
				.orElse(0L) + 1;
	}


	private boolean doesWorldExistWithName(String name) {
		String nameLowerCase = name.toLowerCase(); // Converti il nome fornito in minuscolo
		return list.stream().anyMatch(worldResponse -> worldResponse.getName().toLowerCase().equals(nameLowerCase));
	}

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	private static class NameAlreadyExistsException extends RuntimeException {
		public NameAlreadyExistsException() {
			super("Name already exists");
		}
	}





}
