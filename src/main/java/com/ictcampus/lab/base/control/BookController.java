package com.ictcampus.lab.base.control;

import com.ictcampus.lab.base.control.model.BookRequest;
import com.ictcampus.lab.base.control.model.BookResponse;
import com.ictcampus.lab.base.control.model.WorldRequest;
import com.ictcampus.lab.base.control.model.WorldResponse;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * TODO Add Class Description
 *
 * @author Emilio (FEj) Frusciante - Beta80Group
 * @since 1.0.0
 */

@RestController
@RequestMapping( "/api/v1/books" )
@AllArgsConstructor
public class BookController {

	@GetMapping( value = "", produces = { MediaType.APPLICATION_JSON_VALUE } )
	public List<BookResponse> getBooks() {
		List<BookResponse> list = new ArrayList<>();

		BookResponse bookResponse = new BookResponse();
		bookResponse.setId( 1L );
		bookResponse.setTitle( "Il signore degli agnelli" );
		bookResponse.setAuthor( "J.R.R.Kolkien" );
		bookResponse.setISDB( "6845" );
		bookResponse.setDescription("storia di una pasqua finita male");
		list.add( bookResponse );

		BookResponse bookResponse1 = new BookResponse();

		bookResponse1.setId( 2L );
		bookResponse1.setTitle( "uno nessuno e cento ciro" );
		bookResponse1.setAuthor( "Luigi Pirlandello" );
		bookResponse1.setISDB( "7655" );
		bookResponse1.setDescription("furti di identità napoletane");

		list.add( bookResponse1 );
		return list;
	}

	@GetMapping( value = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE } )
	public BookResponse getBook(
			@PathVariable(name = "id") Long id
	) {
		BookResponse bookResponse = new BookResponse();
		bookResponse.setId( 1L );
		bookResponse.setTitle( "Il signore degli agnelli" );
		bookResponse.setAuthor( "J.R.R.Kolkien" );
		bookResponse.setISDB( "6845" );
		bookResponse.setDescription("storia di una pasqua finita male");
		return bookResponse;
	}

	@PostMapping( value = "", produces = { MediaType.APPLICATION_JSON_VALUE } )
	public Long createBook(
			@RequestBody BookRequest bookRequest
	) {
		BookResponse bookResponse = new BookResponse();
		bookResponse.setId( 1L );
		bookResponse.setTitle( bookRequest.getTitle() );
		bookResponse.setAuthor( bookRequest.getAuthor() );
		bookResponse.setISDB( bookRequest.getISDB() );
		bookResponse.setDescription( bookRequest.getDescription());
		return bookResponse.getId();
	}

	@PutMapping( value = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE } )
	public void editWorld(
			@PathVariable(name = "id") Long id,
			@RequestBody BookRequest bookRequest
	) {
		BookResponse bookResponse = new BookResponse();
		bookResponse.setId( id );
		bookResponse.setTitle( bookRequest.getTitle() );
		bookResponse.setAuthor( bookRequest.getAuthor() );
		bookResponse.setISDB( bookRequest.getISDB() );
		bookResponse.setDescription( bookRequest.getDescription());
	}

	@DeleteMapping( value = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE } )
	public void deleteWorld(
			@PathVariable(name = "id") Long id
	) {
		BookResponse bookResponse = new BookResponse();
		bookResponse.setId( id );
	}

}


